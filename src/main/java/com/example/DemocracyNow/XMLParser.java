package com.example.DemocracyNow;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by asia on 21/02/15.
 */
public class XMLParser {

    public static String XML_FILE = "http://www.democracynow.org/podcast-video.xml";
    public static String GO_TO_ITEM = "/rss/channel/item";

    public static List<VideoData> extractTitleLink() throws Exception {
        // feed the XML link, build and save it as doc
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(XMLParser.XML_FILE);

        // get the first element which is 'rss'
        Node firstElement = doc.getDocumentElement();

        // remove #text
        RemoveHashText(firstElement);

        // create the XPath to get to 'item'
        XPathFactory xPathfactory = XPathFactory.newInstance();
        XPath xpath = xPathfactory.newXPath();
        XPathExpression expr = xpath.compile(XMLParser.GO_TO_ITEM);
        NodeList extractedItem = (NodeList) expr.evaluate(firstElement, XPathConstants.NODESET);

        // create an empty list that will keep objects containing title and link
        List<VideoData> listTitlesLinks = new ArrayList<VideoData>();

        // go though each child ('item') of extractedItem
        for (int j = 0; j < extractedItem.getLength(); j++) {
            Node childInExtractedItem = extractedItem.item(j);
            VideoData newElement = new VideoData();

            for (int l = 0; l < extractedItem.getLength(); l++) {
                Node firstItem = childInExtractedItem;

                // call findTitle and FindVideoLink and add them to the object 'newElement'
                newElement.set_title(findTitle(firstItem));
                newElement.setVideoLink(FindVideoLink(firstItem));
            }

            // add objects to the list
            listTitlesLinks.add(newElement);
        }

        return listTitlesLinks;
      }

    // findTitle finds a title based on the node name 'title'
    public static String findTitle(Node item){
            NodeList childrenOfItem = item.getChildNodes();

            for (int k = 0; k < childrenOfItem.getLength(); k++) {
                Node childOfItem = childrenOfItem.item(k);

                String nodeName = childOfItem.getNodeName();
                if (nodeName.equals("title")) {
                    String title = childOfItem.getTextContent();
                    return title;
                }
            }
        return null;
    }

    // FindVideoLink finds a video link based on the node name 'enclosure' and its attribute 'url'
    public static String FindVideoLink(Node item) {
        NodeList childrenOfItem = item.getChildNodes();

        for (int k = 0; k < childrenOfItem.getLength(); k++) {
            Node childOfItem = childrenOfItem.item(k);

            String nodeName = childOfItem.getNodeName();
            if (nodeName.equals("enclosure")) {
                NamedNodeMap getAttributes = childOfItem.getAttributes();
                Node getVideoLink = getAttributes.getNamedItem("url");

                String videoLink = getVideoLink.getTextContent();
                return videoLink;
            }
        }
        return null;
    }

    public static void RemoveHashText(Node node) throws Exception {

        NodeList listOfNodes = node.getChildNodes();
        // remove #text children from the listOfNodes
        if (node.getNodeName() == "#text" && node.getTextContent().trim().isEmpty()) {
            node.getParentNode().removeChild(node);
        }

        if (listOfNodes != null) {
            // traverse the listOfNodes to create a DOM tree
            for (int i = listOfNodes.getLength() - 1; i >= 0; i--) {
                RemoveHashText(listOfNodes.item(i));
            }
        }
    }
}