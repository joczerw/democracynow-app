/**
 * Tasks:
 *
 *  Use AsyncTask to download the list only once.
 *  When the app is running, think when the app should check if there was an update to the 'list'.
 *  Figure out how it should behave overnight.
 *
 */

package com.example.DemocracyNow;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import java.util.List;

import static com.example.DemocracyNow.R.*;


public class MainScreen extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.main);

        Context context = getApplicationContext();

        // call AsyncTask method (AsyncCheckInternetConnection) to check the Internet connection and
        // pass the return argument 'true/false' to addInternetConnectionMessage
        new AsyncCheckInternetConnection(context){
            @Override
            protected void onPostExecute(Boolean isConnected) {
                addInternetConnectionMessage(isConnected);
            }
        }.execute();
    }

    private void addInternetConnectionMessage(Boolean isConnected){
        // if no Internet connection (isConnected == false), display message
        if (isConnected == false){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("No Internet connection");
            builder.setMessage("Connect to the Internet and try again");
            builder.show();
        }
        // if there is Internet connection (isConnected == true), AsyncTask (AsyncRSSDownloader) is executed
        // a list containing objects (type: VideoData) is saved as thingsForVideos
        if (isConnected == true){
            new AsyncRSSDownloader(){
                @Override
                protected void onPostExecute(List<VideoData> videoData) {
                    addVideoButtons(videoData);
                }
            }.execute();
        }
    }

    private void addVideoButtons(List<VideoData> videoData){
        // create an intent that calls PlayVideo
        final Intent intent = new Intent(this, PlayVideo.class);
        final LinearLayout linearLayout = (LinearLayout) findViewById(id.linearLayout);

        // all the titles and links are removed in order to only keep the most up to date
        // list of titles and links
        linearLayout.removeAllViews();

        for (final VideoData titleAndLink : videoData) {
            // create buttons displaying titles
            Button button = new Button(MainScreen.this);
            button.setTextColor(Color.WHITE);
            button.setBackgroundColor(Color.TRANSPARENT);
            button.setText(titleAndLink.getTitle());

            // set on-click functionality of the button
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // send a video link to the new activity (PlayVideo)
                    intent.putExtra(PlayVideo.videoLink, titleAndLink.getVideoLink());
                    intent.putExtra(PlayVideo.videoTitle, titleAndLink.getTitle());

                    startActivity(intent);
                }
            });
            linearLayout.addView(button);
        }
    }
}