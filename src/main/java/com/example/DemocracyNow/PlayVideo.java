package com.example.DemocracyNow;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

import java.util.Timer;
import java.util.TimerTask;

import static com.example.DemocracyNow.R.*;

public class PlayVideo extends Activity implements MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener {

    VideoView video;
    ProgressDialog progressDialog;
    public static String currentlyPlayingTitle;
    public static String videoTitle = "Video title";
    public static String videoLink = "Video link";
    public static String videoParameters = "Video parameters";
    public static Integer timeIntoVideo;
    public static String finalVideoLink;
    public static Integer timerTime = 0;
    public static Timer timer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("My state: ", "onCreate");

        setContentView(layout.landscape);

        // received title from the MainScreen
        Bundle receivedExtraTitle = getIntent().getExtras();
        currentlyPlayingTitle = receivedExtraTitle.getString(videoTitle);

        // received link from the MainScreen
        Bundle receivedExtraLink = getIntent().getExtras();
        String receivedLink = receivedExtraLink.getString(videoLink);

        finalVideoLink = receivedLink;

        video = (VideoView) findViewById(id.playVideo);

        // create media controller which enables play/pause and going forwards/backwards
        MediaController vidControl = new MediaController(this);
        vidControl.setAnchorView(video);
        video.requestFocus();
        video.setMediaController(vidControl);
        video.setVideoURI(Uri.parse(finalVideoLink));

        // call onError method if an error is encountered
        video.setOnErrorListener(this);

        // create a progress bar to show that the video is loading
        // no message is displayed, just a simple loading spinner
        progressDialog = ProgressDialog.show(this, null, null, true);

        // this magically puts the spinner in the centre of the screen and removes boarders
        progressDialog.setContentView(new ProgressBar(this));

        // this refers to an object of the current class (Play Video) that implements
        // onPreparedListener interface
        video.setOnPreparedListener(this);

        // 'new MediaPlayer.OnCompletionListener()' is a shorthand to create an anonymous class and its object
        // that implements an interface OnCompletionListener
        video.setOnCompletionListener(new MediaPlayer.OnCompletionListener(){
            public void onCompletion(MediaPlayer mp) {
                Log.i("onCompletion ", "I am in onCompletion!");
            }
        });

        video.setOnInfoListener(new MediaPlayer.OnInfoListener(){
            public boolean onInfo(MediaPlayer mp, int what, int extra) {
                Log.i("onInfo ", "I am in onInfo!");
                return false;
            }
        });
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        // once video has been loaded, remove the loading spinner and play video
        progressDialog.dismiss();
        Log.i("Video start ", "I am starting the video!");

        // the code below is used to find a way around an error that was stopping the video from playing
        // timerTime is recorded every 1 sec and used to display the video from the time point where it was stopped
        video.seekTo(timerTime);
        Log.i("Current timer time: ", String.valueOf(timerTime));
        video.start();

        timer = new Timer();
        PlayVideo.timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                timerTime = video.getCurrentPosition();
            }
        }, 0, 1000);
    }

    public boolean onError (MediaPlayer mp, int what, int extra){
        Log.i("Type of error: ", String.valueOf(what));
        Log.i("Error description: ", String.valueOf(extra));

        Integer bufferPercentage = video.getBufferPercentage();
        Log.i("Buffer percentage ", String.valueOf(bufferPercentage));

        timer.cancel();

        // when an error is encountered, Media Player is reset, a link to the interrupted video is passed
        // and the method calls onPreparedListener
        mp.reset();
        video.setVideoURI(Uri.parse(finalVideoLink));
        PlayVideo myPlayVideo = this;
        video.setOnPreparedListener(myPlayVideo);

        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("My state: ", "onPause");

        // get the value which represents how far into the video a user is
        timeIntoVideo = video.getCurrentPosition();
        Log.i("Message", "This is my saved duration: " + timeIntoVideo);

        // save timeIntoVideo for the title of the video currently playing (Shared Preferences)
        SharedPreferences.Editor data = getSharedPreferences(videoParameters, MODE_PRIVATE).edit();
        data.putInt(currentlyPlayingTitle, timeIntoVideo);
        data.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("My state: ", "omResume");

        // retrieve timeIntoVideo (Shared Preferences)
        SharedPreferences data = getSharedPreferences(videoParameters, MODE_PRIVATE);
        Integer restoredData = data.getInt(currentlyPlayingTitle, 0);
        video.seekTo(restoredData);
    }

    @Override
    // together with android:configChanges="orientation|keyboardHidden|screenSize" in the Manifest,
    // this method ensures that the PlayVideo activity is not destroyed when the lock is on
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.i("My state: ", "onConfigurationChanged");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.i("My state: ", "onSaveInstanceState");
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.i("My state: ", "onRestoreInstanceState");
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onStart() {
        Log.i("My state: ", "onStart");
        super.onStart();
    }


}