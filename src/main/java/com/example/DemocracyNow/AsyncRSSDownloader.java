package com.example.DemocracyNow;

import android.os.AsyncTask;

import java.util.List;

public class AsyncRSSDownloader extends AsyncTask <Void, Void, List<VideoData>> {

    @Override
    protected List<VideoData> doInBackground(Void... params){
        // extract title and link from XMLParser and save in extractedTitleLink
        List<VideoData> extractedTitleLink = null;

        try {
            extractedTitleLink = XMLParser.extractTitleLink();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return extractedTitleLink;
    }
}
