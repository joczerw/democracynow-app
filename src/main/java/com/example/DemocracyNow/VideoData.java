package com.example.DemocracyNow;

/**
 * Created by asia on 15/02/15.
 */
public class VideoData {
    String title;
    String videoLink;

    // an empty constructor
    public VideoData(){
    }

    public VideoData(String argTitle, String argVideoLink){
        title = argTitle;
        videoLink = argVideoLink;
    }

    public void set_title(String argTitle){
        title = argTitle;
    }

    public void setVideoLink(String argVideoLink){
        videoLink = argVideoLink;
    }

    // return video title
    public String getTitle(){
        return title;
    }

    // return video link
    public String getVideoLink(){
        return videoLink;
    }
}
