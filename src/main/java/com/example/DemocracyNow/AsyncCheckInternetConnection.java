package com.example.DemocracyNow;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

/**
 * Created by Asia on 26/03/2016.
 */

public class AsyncCheckInternetConnection extends AsyncTask <Void, Void, Boolean> {

    Context objectContext;

    // constructor
    public AsyncCheckInternetConnection(Context context){
        objectContext = context;
    }

    // check the Internet connection
    @Override
    protected Boolean doInBackground(Void... params) {
        ConnectivityManager connectivityManager = (ConnectivityManager)objectContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        if (isConnected == true){
            return true;
        }

        else {
            return false;
        }
    }
}
